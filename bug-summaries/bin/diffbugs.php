<?php

include('config.php');

$bin=dba_open(DB_DIR."/bin.db", "r", "db4");
$oldbuglist=dba_open($argv[1], "r", "db4");
$newbuglist=dba_open($argv[2], "r", "db4");

$keys=array();

foreach(array($oldbuglist, $newbuglist) as $id) {
  $key = dba_firstkey($id);
  while ($key != false) {
    $keys{$key}=1;
    $key = dba_nextkey($id);
  }
}


ksort($keys);
foreach($keys as $k => $v) {
  if (dba_fetch($k, $oldbuglist) == dba_fetch($k, $newbuglist)) continue;
  if (preg_match("/[^0-9]/", $k)) continue;
  $old = unserialize(dba_fetch($k, $oldbuglist));
  $new = unserialize(dba_fetch($k, $newbuglist));
  if (!$old) { bugdiff($new, "new"); continue; }
  if (!$new) { bugdiff($old, "solved"); continue; }
  if ($new{'-'} && !$old{'-'}) { bugdiff($new, "removed from testing"); continue; }
  if (!$new{'-'} && $old{'-'}) { bugdiff($new, "back in testing"); continue; }
  $diff=array();
  foreach(array('+' => 'patch', 'P' => 'pending', 'H' => 'help', 'S' => 'security', '=' => 'merged') as $t => $s) {
    if ($new{$t} && !$old{$t}) array_push($diff, "+$s");
    if (!$new{$t} && $old{$t}) array_push($diff, "-$s");
  }
  if ($diff) bugdiff($new, implode(", ", $diff));
}

if (!$isChanged) {
 print "<p>No changes at ";
 print date("D M j G:i:s T Y");
 print "</p>\n";
} else {
 print "</ul>";
}

function bugdiff($bug, $what) {
 global $bin;
 global $isChanged;
 if (!$isChanged) {
   $isChanged=1;
   print "<p>Changes at ";
   print date("D M j G:i:s T Y");
   print ":</p>\n<ul>";
 }
 $bugpt = array();
 foreach($bug{'p'} as $k)
  if (dba_fetch($k, $bin))
   array_push($bugpt, "<a href=\"https://tracker.debian.org/pkg/".dba_fetch($k, $bin)."\">".$k."</a>");
  else
   array_push($bugpt, $k);
 print "<li><a href=\"http://bugs.debian.org/";
 print $bug{'i'};
 print "\">";
 print $bug{'i'};
 print "</a> ";
 print $what;
 print ": ";
 if ($bug{'p'}) print implode(", ", $bugpt);
 print " ";
 print $bug{'t'};
 print "\n";
}
?>
