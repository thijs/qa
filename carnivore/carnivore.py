#!/usr/bin/python
# -*- coding: utf-8 -*-

# Utility functions to use carnivore
# Copyright (C) 2006  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os, sys, bsddb
import apt_pkg

carnivore = bsddb.btopen("/srv/qa.debian.org/carnivore/result.db", "r")

def join(sep, items):
    return reduce(lambda a, b: a+sep+b, items)

class carnivoreEntry:
    def __init__(self, v):
        if not isinstance(v, tuple):
            raise "Not a carnivore typle"

        self.id = v[0]
        (self.ldap, self.realname, self.gpg, self.email, self.package) = [], [], [], [], []
        (self.extra, self.expl, self.warnings, self.mia) = [], [], [], []
        self.gecos = ""
        self.keyring = {'keyring': [], 'emeritus': [], 'removed': [], 'ldap': [], 'dm': []}
        for item in set(v[1]+v[2]):
            if item.startswith("ldap:"):
                if item[5:] == "rhonda": # "primary" uid, put in front
                    self.ldap.insert(0, item[5:])
                else:
                    self.ldap.append(item[5:])
            elif item.startswith("email:"):
                self.email.append(item[6:])
            elif item.startswith("realname:"):
                self.realname.append(item[9:])
            elif item.startswith("maint:"):
                self.package.append(item[6:])
            elif item.startswith("gpg:"):
                self.gpg.append(item[4:])
            elif item.startswith("x:"):
                self.expl.append(item[2:])
                if item.startswith("x:ldap:gpg:"):
                    self.keyring['ldap'].append(item[11:].split(':')[1])
                if item.startswith("x:gpg:keyring:"):
                    gpg, ring = item[14:].split(':')
                    self.keyring[ring].append(gpg)
                if item.startswith("x:ldap:realname:"):
                    self.gecos = item[16:].split(':')[1]
            else:
                self.extra.append(item)
        for ring in self.keyring.values(): ring.sort()

    def __repr__(self):
        if self.ldap:
            return self.ldap[0]
        return self.email[0]

    def getPrettyprintedText(self):
        text = ""
        if self.ldap: text += "DD: "+self.gecos+" <"+self.ldap[0]+"@debian.org>\n"
        if self.realname: text += "Known as: "+", ".join(self.realname)+"\n"
        if self.email: text += "Using emails: "+", ".join(self.email)+"\n"
        for k, v in self.keyring.iteritems():
            for key in v:
                text += "Key in "+k+": "+key+"\n"
        i = self.ldap[0] if self.ldap else self.email[0]
        text += "Links: https://qa.debian.org/developer.php?login={0} — ".format(i)
        text += "https://contributors.debian.org/contributors/mia/query?q={}\n".format(i)
        if self.ldap:
            text += "NM: https://nm.debian.org/person/{0}\n".format(i)
        else:
            text += "NM: https://contributors.debian.org/contributors/mia/query?q={}&to=nm\n".format(i)
        p = "0"
        if self.package and len(self.package) <= 5:
            p = "%s (%s)" % (len(self.package), ", ".join(self.package))
        elif self.package:
            p = "%s (%s)" % (len(self.package), ", ".join(self.package[:4]+["..."]))
        text += "Packages: %s\n" % p
        if self.extra:
            for i in self.extra:
                text += i + "\n"

        return text

def search(key):
    if not carnivore.has_key(key):
        return []
    while 1:
        lastkey = key
        key = carnivore[key]
        if key[0] == "(":
            return [lastkey]
        elif key[0] == "[":
            return eval(key)

def get(key):
    while 1:
        key = carnivore[key]
        if key[0] == "(":
            return carnivoreEntry(eval(key))
        elif key[0] == "[":
            raise "%s is not an id of a real carnivore entry" % key

class iteritems:
    def __init__(this):
        this.iter = carnivore.iteritems()
    def __iter__(this): return this
    def next(this):
        while 1:
            key, val = this.iter.next()
            if val[0] == '(':
                return key, carnivoreEntry(eval(val))

# vim: et
