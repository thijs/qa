#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
 Generates the orphan.txt file used by orphaned.wml.
 This version is basically a complete rewrite from the previous version
 and was changed to use the SOAP interface to query the BTS instead of the LDAP
 bridge.
 It requires the following Debian packages:

 * Python 2.6
 * python-soappy

 Please change paths configured below if necessary
"""

# Copyright (C) 2001, 2002, 2003, 2004 Martin Michlmayr <tbm@cyrius.com>
#               2011 Arno Töll <debian@toell.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.


from __future__ import print_function

import sys
import re
import string
import SOAPpy
import time
import os


MAINTAINER_FILE = '/srv/qa.debian.org/data/ftp/Maintainers'
OUTPUT_FILE = '/srv/qa.debian.org/data/orphaned.txt'
URL = 'https://bugs.debian.org/cgi-bin/soap.cgi'
NAMESPACE = 'Debbugs/SOAP'

ca_path = '/etc/ssl/ca-debian'
if os.path.isdir(ca_path):
	os.environ['SSL_CERT_DIR'] = ca_path

###
### Nothing else should be changed below
###

class OrphanedPackage( object ):
	def __repr__( self ):
		return( "%s\n%s\n%d\n%s\n%s\n%d\n\n"
			% ( self._package, self._maintainer, self._bugid, self._title, self._reporter, self._age ) )

	def __init__( self, package, maintainer, bugid, title, reporter, age ):
		self._package = package
		self._maintainer = maintainer
		self._bugid = bugid
		self._title = title
		self._reporter = reporter
		self._age = age


try:
	input = open( MAINTAINER_FILE, 'r' )
except ( IOError, OSError ) as e:
	print( "Couldn't access maintainer file `%s': %s" % ( MAINTAINER_FILE, e ) )
	sys.exit( 1 )


maintainers = { }
orphaned = []

for line in input.readlines():
	line = string.rstrip( re.sub( '\s+', ' ', line ) )
	package, maintainer = string.split( line, ' ', 1 )
	maintainers[package] = maintainer

input.close()

raw_bugs = []
try:
	server = SOAPpy.SOAPProxy( URL, NAMESPACE, simplify_objects = 1 )
	buglist = server.get_bugs( "package", "wnpp" );
	for i in range(0, len(buglist), 500):
		bugs_slice = server.get_status(buglist[i:i+500])

		if ( not bugs_slice or 'item' not in bugs_slice ):
			print( "Could not retrieve bugs (but no apparent error occurred)\n" )
			sys.exit(0)

		# Force argument to be a list, SOAPpy returns a dictionary instead of a
		# dictionary list if a single bug was found. Not this were likely after
		# all :>
		if ( not isinstance( bugs_slice['item'], list ) ):
			raw_bugs.append(bugs_slice['item'])
		else:
			raw_bugs.extend(bugs_slice['item'])
except ( Exception ) as e:
	print( "Can't access SOAP interface on %s (ns: %s): %s" % ( URL, NAMESPACE, e ) )
	sys.exit( 1 )


for bugs in raw_bugs:
	current_bug = bugs['value']

	# Skip bugs being done or archived
	if ( len(current_bug['done']) or current_bug['archived']) :
		continue

	# Skip merged bugs which have an older counterpart
	if ( current_bug['mergedwith'] ):
		merged_bug = int( min( str.split(str(current_bug['mergedwith']) ) ) )
		# This is the "jwilk hack(TM)". This is, to add bugs nonetheless which are
		# duplicates of older ones, but the older is not tagged Orphaned ("O:")
		for remaining_bugs in raw_bugs:
			if ( remaining_bugs['key'] == merged_bug ):
				merged_bug = remaining_bugs['value']
				break
		else:
			print("Could not retrieve expected duplicate bug in WNPP list\n")
			sys.exit(1)

		if ( current_bug['id'] > merged_bug['id'] and merged_bug['subject'].startswith("O:") ):
			continue

	# Skip packages with illegal subject line
	parsed_subject = re.match( "(\w+):\s+(\S+)", current_bug['subject'] )
	if ( not parsed_subject ):
		continue

	# Skip Non-O bugs
	#if ( parsed_subject.group( 1 ) not in ( "O", "RFA" ) ):
	if ( parsed_subject.group( 1 ) != "O" ):
		continue

	package = parsed_subject.group( 2 ).lower()

	if ( not len( package ) ):
		continue

	# Skip packages where the maintainer is not listed in the maintainers file
	if ( package not in maintainers ):
		continue

	# Skip packages already owned by the QA team
	if ( string.count( maintainers[package], 'debian-qa@lists.debian.org' ) or
		string.count( maintainers[package], 'packages@qa.debian.org' ) ):
		continue

	orphaned.append( OrphanedPackage( package = package,
										maintainer = maintainers[package],
										bugid = current_bug['id'],
										title = current_bug['subject'],
										reporter = current_bug['originator'],
										age = int( time.time() - current_bug['date'] ) / 86400
									) )


try:
	output = open( OUTPUT_FILE, 'w' )
except ( IOError, OSError ) as e:
	print( "Output file not accessible `%s': %s" % ( OUTPUT_FILE, e ) )
	sys.exit( 1 )


output.write( str( len( orphaned ) ) + "\n\n" )
for orphaned_package in sorted( orphaned, key = lambda x: x._age, reverse=True ):   # sort by age:
	output.write( str( orphaned_package ) )

# vim: ts=4:expandtab:shiftwidth=4:
