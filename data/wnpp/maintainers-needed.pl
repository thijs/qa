#!/usr/bin/perl -w

# Post a summary of orphaned packages, those up for adoption and needing help
# Copyright (C) 2000, 2001, 2002, 2003  Marcelo E. Magallon <mmagallo@debian.org>
# Copyright (C) 2004, 2005  Martin Michlmayr <tbm@cyrius.com>
# Copyright (C) 2004  Frank Lichtenheld <djpig@debian.org>
# Copyright (C) 2005, 2006  Christoph Berg <myon@debian.org>
# Copyright (C) 2011  David Prévot <david@tilapin.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

use strict;
use warnings;

use SOAP::Lite;
use Date::Parse;
use Mail::Header;
use DB_File;

my $whereto = "debian-devel\@lists.debian.org";
my $wnpp_url = "http://www.debian.org/devel/wnpp/";
my $rfa_url = $wnpp_url . "rfa_bypackage";
my $orphaned_url = $wnpp_url . "orphaned";
my $rfh_url = $wnpp_url . "help_requested";

my $new_only = 1; # show only new WNPP entries from the previous week

my $mail;
if (@ARGV && $ARGV[0] eq "--email") {
    $mail = "| /usr/sbin/sendmail -t";
} else {
    $mail = "> /dev/stdout";
}
#$mail = "> report.txt";

my $GETPACKAGES = "/srv/qa.debian.org/data/ftp/get-packages";
my @UNS_ARCHS = glob "/srv/qa.debian.org/ftp/debian/dists/unstable/main/binary-*";
my $UNS_ARCHS = "@UNS_ARCHS";
$UNS_ARCHS =~ s,/srv.*?binary-,,g;
@UNS_ARCHS = split / /, $UNS_ARCHS;
my $POPCONDB = "/srv/qa.debian.org/data/popcon/popcon.db";

# Work around SOAP::Lite not being able to verify certs correctly
my $ca_dir = '/etc/ssl/ca-debian';
$ENV{PERL_LWP_SSL_CA_PATH} = $ca_dir if -d $ca_dir;

my $soap = SOAP::Lite->uri('Debbugs/SOAP')->proxy('https://bugs.debian.org/cgi-bin/soap.cgi')
       or die "Couldn't make connection to SOAP interface: $@";
my $bugs = $soap->get_bugs(package=>'wnpp')->result;

my $status = {};
while (my @slice = splice(@$bugs, 0, 500)) {
    my $tmp = $soap->get_status(@slice)->result() or die;
    %$status = (%$status, %$tmp);
}

my $curtime = time;

my (@rfa, @orphaned, @rfh, %pkginfo);
ALLPKG: foreach my $bugnr (keys %$status) {
    next if $status->{$bugnr}->{done};
    next if $status->{$bugnr}->{archived};
    my $title = $status->{$bugnr}->{subject};
    if (! $title) {
        print $bugnr, "\n";
        next;
    }
    next unless $title =~ /^(?:RFA|ITO|O|RFH):/;
    my $mergedwith = $status->{$bugnr}->{mergedwith};
    foreach my $merged (split ' ',$mergedwith) {
          next ALLPKG if int($merged) < int($bugnr);
    }
    my $date = $status->{$bugnr}->{date};
    my $age = ($curtime - $date)/86400;
    # there are all kinds of broken bug titles; one common type are bugs
    # without a description separated from the package name by --.  If
    # there's no dash, there can be 3 different cases:
    #  1. tag: package
    #  2. tag: package1, package2, package3
    #  3. tag: word word word
    # While all of them are ill-formed, we will accept the first and
    # second case.  However, we will ignore bugs following the third
    # schema since we don't know whether the words are packages or
    # a package name followed by a description (see e.g. #319801 or
    # http://lists.debian.org/debian-devel/2005/07/msg01491.html).
    # Therefore, we ignore lines which contain no dash and no comma
    # but which have a space.
    my $tmp = $title;
    $tmp =~ s/^[^:]+:\s+//; # get rid of the tag
    if ($tmp !~ /-/ && $tmp !~ /,/ && $tmp =~ /\s/) {
        next;
    }
    $title =~ s/\s+-+\s+(.*)$//;
    my $desc = $1;
    if ($title =~ /^(?:wnpp:?\s*)?(?:RFA|ITO):\s*((?:[a-z0-9][-a-z0-9+.]+(?:,?\s+)?)+)/io) {
        foreach my $package (split /,?\s+/, $1) {
            next if (grep { $_ eq $package } @rfa);
            $pkginfo{$package} = {
                id => $bugnr, age => $age, desc => $desc, revdeps => {} };
            push @rfa, $package;
        }
    } elsif ($title =~ /^O:\s*((?:[a-z0-9][-a-z0-9+.]+(?:,?\s+)?)+)/io) {
        foreach my $package (split /,?\s+/, $1) {
            next if (grep { $_ eq $package } @orphaned);
            $pkginfo{$package} = {
                id => $bugnr, age => $age, desc => $desc, revdeps => {} };
            push @orphaned, $package;
        }
    } elsif ($title =~ /^RFH:\s*((?:[a-z0-9][-a-z0-9+.]+(?:,?\s+)?)+)/io) {
        foreach my $package (split /,?\s+/, $1) {
            next if (grep { $_ eq $package } @rfh);
            $pkginfo{$package} = {
                id => $bugnr, age => $age, desc => $desc, revdeps => {} };
            push @rfh, $package;
        }
    }
}

$/ = "";
if (open PCKG, "$GETPACKAGES -s unstable -a source|") {
      PCKG: while (<PCKG>) {
        next if /^\s*$/;
        my ($nonfree, $src, $maint, $pri, $id, @bin) = (0);
        chomp;
        s/\n\s+/\376\377/g;
        s/\376\377\s*\376\377/\376\377/og;
        while (/^(\S+):\s*(.*)\s*$/mg) {
            my ($key, $value) = ($1, $2);
            $key =~ tr [A-Z] [a-z];
            $value =~ s/\376\377/\n /g;
            if ($key eq "package") {
                $src = $value;
            } elsif ($key eq "binary") {
                map { m/^([a-z0-9][-+.a-z0-9]+)/; push @bin, $1 } 
                    split /[,|]\s*/, $value;
            } elsif ($key eq "priority") {
                $pri = $value;
            } elsif ($key eq "section" && $value =~ m/non-free/) {
                $nonfree = 1;
            } elsif ($key eq "maintainer") {
                $maint = $value;
            }
        }

        foreach $id ($src, @bin) {
            #next unless !defined($pkginfo{$id});
            $pkginfo{$id} = \%{$pkginfo{$src}}
                if ($src ne $id && !defined($pkginfo{$id}));
            $pkginfo{$id}{pri} = $pri;
            $pkginfo{$id}{nonfree} = $nonfree;
            $pkginfo{$id}{maint} = $maint;
        }
    }
    close PCKG or warn "Couldn't close sources file: $!";;
} else {
    warn "Couldn't open sources file: $!";
}

foreach my $arch (@UNS_ARCHS) {
    if (open PCKG, "$GETPACKAGES -s unstable -a $arch |") {
      PCKG: while (<PCKG>) {
        next if /^\s*$/;
        my ($nonfree, $pkg, $pri, $src, $id, $section, @deps) = (0);
        chomp;
        s/\n\s+/\376\377/g;
        s/\376\377\s*\376\377/\376\377/og;
        while (/^(\S+):\s*(.*)\s*$/mg) {
            my ($key, $value) = ($1, $2);
            $key =~ tr [A-Z] [a-z];
            $value =~ s/\376\377/\n /g;
            if ($key eq "package") {
                $pkg = $value;
            } elsif ($key =~ m/depends/) {
                map { m/^([a-z0-9][-+.a-z0-9]+)/; push @deps, $1 }
                    split /[,|]\s*/, $value;
            }
        }

        foreach (@deps) {
            $pkginfo{$_}{revdeps}{$pkg} = 1 if defined($pkginfo{$_});
        }
      }
      close PCKG or warn "Couldn't close packages file for $arch: $!";
    } else {
        warn "Couldn't open packages file for $arch: $!";
    }
}
$/ = "\n";

my ($orphaned, $rfa, $new_orphaned, $new_rfa, $rfh, $new_rfh) = (0, 0, 0, 0, 0, 0);

foreach my $pkg (@orphaned) {
    next unless %{$pkginfo{$pkg}};
    $orphaned++;
    $new_orphaned++ if ($pkginfo{$pkg}{age} < 7);
}
foreach my $pkg (@rfa) {
    next unless %{$pkginfo{$pkg}};
    $rfa++;
    $new_rfa++ if ($pkginfo{$pkg}{age} < 7);
}
foreach my $pkg (@rfh) {
    next unless %{$pkginfo{$pkg}};
    $rfh++;
    $new_rfh++ if ($pkginfo{$pkg}{age} < 7);
}

my ($mday, $mon, $year) = (localtime)[3..5];
my $date = (qw/Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec/)[$mon] . 
    " $mday, " . ($year+1900);

my $new_message = "";
if ($new_only) {
    $new_message = " in the\nlast week";
}
open NFM, $mail or die "Couldn't open output stream: $!";
print NFM <<EOH;
From: wnpp\@debian.org
To: $whereto
Subject: Work-needing packages report for $date
X-Mailer: maintainers-needed.pl
X-Flames-to: debian-qa\@lists.debian.org
Mail-Followup-To: debian-devel\@lists.debian.org

The following is a listing of packages for which help has been requested
through the WNPP (Work-Needing and Prospective Packages) system$new_message.

Total number of orphaned packages: $orphaned (new: $new_orphaned)
Total number of packages offered up for adoption: $rfa (new: $new_rfa)
Total number of packages requested help for: $rfh (new: $new_rfh)

Please refer to $wnpp_url for more information.

EOH
;

# open db containing popcon data
my %popcon;
my $popcon_db = tie %popcon, "DB_File", $POPCONDB, O_RDONLY
    or warn "Can't open database $POPCONDB: $!";

if ($orphaned) {
    print NFM "-"x72, "\n\n";
    if ($new_only) {
        my $old = $orphaned - $new_orphaned;
        if ($new_orphaned) {
            print NFM "The following packages have been orphaned:\n";
            pkgs_out( "orphaned", \@orphaned, 1 );
            if ($old) {
                print NFM "\n";
                print NFM "$old older packages have been omitted from this listing, see\n";
                print NFM "$orphaned_url for a complete list.\n";
            }
        } else {
            print NFM "No new packages have been orphaned, but a total of $old packages are\n";
            print NFM "orphaned.  See $orphaned_url\n";
            print NFM "for a complete list.\n";
        }
    } else {
        print NFM "The following packages are orphaned:\n";
        pkgs_out( "orphaned", \@orphaned, 0 );
    }
}

if ($rfa) {
    print NFM "\n", "-"x72, "\n\n";
    if ($new_only) {
        my $old = $rfa - $new_rfa;
        if ($new_rfa) {
            print NFM "The following packages have been given up for adoption:\n";
            pkgs_out( "offered", \@rfa , 1);
            if ($old) {
                print NFM "\n";
                print NFM "$old older packages have been omitted from this listing, see\n";
                print NFM "$rfa_url for a complete list.\n";
            }
        } else {
            print NFM "No new packages have been given up for adoption, but a total of $old packages\n";
            print NFM "are awaiting adoption.  See $rfa_url\n";
            print NFM "for a complete list.\n";
        }
    } else {
        print NFM "The following packages are up for adoption:\n";
        pkgs_out( "offered", \@rfa, 0 );
    }
}

if ($rfh) {
    print NFM "\n", "-"x72, "\n\n";
    # There are still so few RFHs that $new_only is not taken into account
    # here.
    print NFM "For the following packages help is requested:\n";
    pkgs_out( "requested", \@rfh, 0 );
    print NFM "\n";
    print NFM "See $rfh_url for more information.\n";
}

close NFM or warn "Couldn't close output stream: $!";

exit 0;

### subroutines ###

sub fmt {
    ($_, my $length, my $prefix) = @_;
    $length = 72 if (!$length);
    $prefix = "" if (!$prefix);
    $length -= length($prefix);
    my $s = "";
    my $parindent = "";
    while (/\G(.{1,$length}\S)(?:\s+|$)/g) {
        $s .= $prefix . $parindent . $1 . "\n";
        $parindent = "  ";
    }
    chomp $s;
    return $s;
}

sub sort_pkgs {
    return $a cmp $b if ($pkginfo{$a}{age} < 7 && $pkginfo{$b}{age} < 7)
	|| ($pkginfo{$a}{age} >= 7 && $pkginfo{$b}{age} >= 7);
    return $pkginfo{$a}{age} <=> $pkginfo{$b}{age};
}

sub pkgs_out {
    my ($date_str, $list, $new_only) = @_;
    foreach my $pkg (sort sort_pkgs @$list) {
	next unless %{$pkginfo{$pkg}};
	# Joy, you owe me, I hate this constructs!
    $pkginfo{$pkg}{age} = int($pkginfo{$pkg}{age});
	my $age = ($pkginfo{$pkg}{age} == 0) ? "today" : 
	    ($pkginfo{$pkg}{age} == 1) ? "yesterday" :
            "$pkginfo{$pkg}{age} days ago";
    next if $new_only && $pkginfo{$pkg}{age} >= 7;
    if ($new_only) {
        print NFM "\n", " "x3;
    } else {
        print NFM "\n", ($pkginfo{$pkg}{age} < 7) ? "[NEW] " : " "x3;
    }
	print NFM
	    "$pkg (#$pkginfo{$pkg}{id}), $date_str $age",
	    $pkginfo{$pkg}{nonfree}?" (non-free)\n":"\n";
	print NFM
	    fmt("Description: " . $pkginfo{$pkg}{desc}, 72, " "x5), "\n"
	    if defined($pkginfo{$pkg}{desc});
#	print NFM
#	    "     Current maintainer: ", $pkginfo{$pkg}{maint}, "\n"
#	    if defined($pkginfo{$pkg}{maint});
    if (%{$pkginfo{$pkg}{revdeps}}) {
        my @revdeps = sort keys %{$pkginfo{$pkg}{revdeps}};
        if ((my $rest = @revdeps - 10) > 0) {
            splice @revdeps, 10, $rest, ("($rest more omitted)");
        }
        print NFM
            fmt("Reverse Depends: " . join(' ', @revdeps), 72, " "x5),
            "\n";
    }
    if (my $p = $popcon{"p:$pkg"}) {
        print NFM fmt("Installations reported by Popcon: $p", 72, " "x5), "\n";
    }
    print NFM
        fmt("Bug Report URL: http://bugs.debian.org/" . $pkginfo{$pkg}{id}, 72, " "x5), "\n";
    }
}


# vim: ts=4:expandtab:shiftwidth=4:
