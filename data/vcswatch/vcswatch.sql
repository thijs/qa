--CREATE EXTENSION IF NOT EXISTS debversion;

CREATE TYPE vcsstatus AS ENUM (
	'COMMITS',
	'ERROR',
	'NEW',
	'OK',
	'OLD',
	'TODO',
	'UNREL'
);

CREATE TABLE vcs (
	package text PRIMARY KEY,
	package_version text NOT NULL, -- or debversion
	vcs text,
	url text,
	branch text,
	browser text,
	last_scan timestamptz,
	next_scan timestamptz DEFAULT now(),
	status vcsstatus DEFAULT 'TODO',
	debian_dir boolean DEFAULT true, -- debian/changelog or changelog
	changelog_version text, -- or debversion
	changelog_distribution text,
	changelog text,
	error text,
	valid_checkout boolean NOT NULL DEFAULT false,
	edited_by text,
	edited_at timestamptz,
	tag text,
	commits int,
	hash text,
	vcslog text
);

CREATE INDEX ON vcs (next_scan);

GRANT SELECT ON vcs TO PUBLIC;
GRANT UPDATE (vcs, url, branch, next_scan, valid_checkout, edited_by, edited_at) ON vcs TO PUBLIC;

CREATE OR REPLACE FUNCTION round_time(timestamptz) RETURNS timestamptz AS
$$SELECT date_trunc('hour', $1) + '1 min'::interval * ceil(date_part('minute', $1) / 1.0)$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION next_scan() RETURNS timestamptz AS
$$SELECT round_time(now() + '20h'::interval + '8h'::interval * random())$$
LANGUAGE SQL;
