#!/usr/bin/python
#  Copyright (C) 2004 Igor Genibel                                         
#  Copyright (C) 2005, 2006, 2008, 2010 Christoph Berg <myon@debian.org>
#                                                                          
#  This program is free software; you can redistribute it and/or           
#  modify it under the terms of the GNU General Public License             
#  as published by the Free Software Foundation; either version 2          
#  of the License, or (at your option) any later version.                  
#                                                                          
#  This program is distributed in the hope that it will be useful,         
#  but WITHOUT ANY WARRANTY; without even the implied warranty of          
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           
#  GNU General Public License for more details.                            

# This is extract the information from popcon for each source package

import sys, os, re, fileinput, dbm, bsddb
prefix = "/srv/qa.debian.org/data/ddpo/results"
sources_hash = {}
debian_package = {}

def extract_source():
    """Map source to binary packages"""
    bin2src = bsddb.btopen(prefix + "/archive.db", 'r')
    for package in bin2src:
        if package[0:4] == 'bin:':
            source = package[4:]
            sources_hash[source] = bin2src[package].split(" ")
            for binary in sources_hash[source]:
                debian_package[binary] = 1

def process_rank(name, n, reg, pack_db):
    """Extract data from by_name file in order to feed the rank dictionaries"""
    popcon = open("by_" + name)
    reg1 = re.compile(reg)

    while 1:
        line = popcon.readline()
        if not line:
            break
        if not reg1.search(line):
            continue
        (rank, binary, nr) = reg1.search(line).groups()

        if not debian_package.has_key(binary):
            continue

        pack_db['p'+n+':'+binary] = nr
        pack_db['p'+n+'r:'+binary] = rank

        if int(rank) == 1:
            pack_db['p'+n+'max:'] = nr
            pack_db['p'+n+'max:n'] = binary

def process_popcon(pack_db):
    """Extract binary data from by_* files (provided by popcon)"""
    process_rank('inst', 'i',     '^(\d+)\s+(\S+)\s+(\d+)\s+\d+\s+\d+\s+\d+\s+\d+', pack_db)
    process_rank('vote', 'v',     '^(\d+)\s+(\S+)\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+', pack_db)
    process_rank('old', 'o',      '^(\d+)\s+(\S+)\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+', pack_db)
    process_rank('recent', 'r',   '^(\d+)\s+(\S+)\s+\d+\s+\d+\s+\d+\s+(\d+)\s+\d+', pack_db)
    process_rank('no-files', 'n', '^(\d+)\s+(\S+)\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+)', pack_db)

def find_max(n, pack_db, max_db):
    """Find maximum over binaries for each source"""
    for source in sources_hash.keys():
        max_inst = 0
        for binary in sources_hash[source]:
            if pack_db.has_key('p'+n+':'+binary):
                if int(pack_db['p'+n+':'+binary]) >= max_inst:
                    max_inst = int(pack_db['p'+n+':'+binary])
                    pack_db['s'+n+':'+source] = "%s" % max_inst
                    max_db[source] = "%s" % max_inst
        # be compatible
        if n == 'i':
            pack_db['p:'+source] = "%s" % max_inst

if __name__ == "__main__":
    extract_source()
    pack_db = {}
    process_popcon(pack_db)
    max_db = {}
    find_max('i', pack_db, max_db)
    find_max('v', pack_db, max_db)
    find_max('o', pack_db, max_db)
    find_max('r', pack_db, max_db)
    find_max('n', pack_db, max_db)
    db = dbm.open("popcon-new", 'c')
    for key in pack_db:
        db[key] = pack_db[key]
    db.close()
    max_file = open("popcon-new", 'w')
    for key in max_db:
        print >> max_file, "%s: %s" % (key, max_db[key])
    max_file.close()

# vim:et:sw=4:
