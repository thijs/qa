#!/usr/bin/perl -w

#    extract_carnivore.pl - extract data from carnivore for use in ddpo
#    Copyright (C) 2011-2014 Bart Martens <bartm@knars.be>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use DBI;
use DB_File;

my $dbh = DBI->connect( "dbi:Pg:service=udd", "guest" ) or die;

my $db_filename = "carnivore-new.db";
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
	or die "Can't open database $db_filename : $!";
my $sth;
my $separator = ":";

$sth = $dbh->prepare( "select id, email from carnivore_emails" );
$sth->execute or die;

my %emaillc2id;
my %email2id;
my %id2email;

while( my $row = $sth->fetchrow_hashref )
{
	my $email = $row->{'email'};
	my $emaillc = lc( $row->{'email'} );
	my $id = $row->{'id'};

	if( $email =~ /$separator/ )
	{
		warn "email contains separator ($email)";
		next;
	}

	if( $email !~ /@/ )
	{
		#warn "email contains no at-sign ($email)";
		next;
	}

	push @{$id2email{$id}}, $email;

	if( defined $email2id{$email} and $email2id{$email} != $id )
	{
		warn "email with multiple id's ($email)";
		next;
	}

	$email2id{$email} = $id;
	$emaillc2id{$emaillc} = $id if( not defined $emaillc2id{$emaillc} );
	$emaillc2id{$emaillc} = $id if( $id > $emaillc2id{$emaillc} );
}

$sth = $dbh->prepare( "select id, key from carnivore_keys" );
$sth->execute or die;

my %id2key;
my %id2fpr;
my %key2id;

while( my $row = $sth->fetchrow_hashref )
{
	my $key = $row->{'key'};
	my $id = $row->{'id'};

	if( $key =~ /$separator/ )
	{
		warn "key contains separator ($key)";
		next;
	}

	if( $key !~ /^[0-9A-F]*$/ )
	{
		warn "key contains bad character(s) ($key)";
		next;
	}

	if( length( $key ) !~ /^(32|40)$/ )
	{
		warn "key has wrong length ($key)";
		next;
	}

	$id2fpr{$id}{$key} = 1;

	$key = substr( $key, length( $key ) - 8, 8 );

	if( defined $key2id{$key} and $key2id{$key} != $id )
	{
		warn "key not unique ($key)";
		next;
	}

	$key2id{$key} = $id;
	$id2key{$id}{$key} = 1;
}

$sth = $dbh->prepare( "select id, name from carnivore_names" );
$sth->execute or die;

my %id2name;

while( my $row = $sth->fetchrow_hashref )
{
	my $name = $row->{'name'};
	my $id = $row->{'id'};

	$name =~ s/^Maintainer: //;
	$name =~ s/ *\[Debian Key\]//;
	$name =~ s/  */ /g;

	if( $name =~ /$separator/ )
	{
		warn "name contains separator ($name)";
		next;
	}

	push @{$id2name{$id}}, $name;
}

foreach my $email ( keys %email2id )
{
	my $emaillc = lc( $email );
	my $source = $email2id{$email};
	my $target = $emaillc2id{$emaillc};

	next if( $source eq $target );

	if( defined $id2email{$source} )
	{
		push @{$id2email{$target}}, @{$id2email{$source}};
		delete $id2email{$source};
	}

	if( defined $id2fpr{$source} )
	{
		@{$id2fpr{$target}}{keys %{$id2fpr{$source}}} = values %{$id2fpr{$source}};
		delete $id2fpr{$source};
	}

	if( defined $id2key{$source} )
	{
		foreach my $key ( keys %{$id2key{$source}} )
		{
			$key2id{$key} = $target;
			#$db{"key2id:$key"} = $target;
		}
	}

	if( defined $id2name{$source} )
	{
		push @{$id2name{$target}}, @{$id2name{$source}};
		delete $id2name{$source};
	}

	$email2id{$email} = $target;
}

foreach my $emaillc ( keys %emaillc2id )
{
	$db{"email2id:$emaillc"} = $emaillc2id{$emaillc};
}

foreach my $id ( keys %id2email )
{
	my %seen;
	my @unique;
	foreach my $email ( @{$id2email{$id}} )
	{
		if( $email !~ m%^([^@]+)@([^@]+)$% )
		{
			push @unique, $email;
			next;
		}
		my $emailfix = "$1@".lc($2);
		my $emaillc = lc($email);

		next if( defined $seen{$emailfix} );
		$seen{$emailfix} = 1;

		push @unique, $email;
	}
	@{$id2email{$id}} = @unique;

	$db{"email:$id"} = join( $separator, @{$id2email{$id}} );
}

foreach my $id ( keys %id2fpr )
{
	$db{"fpr:$id"} = join( $separator, keys %{$id2fpr{$id}} );
}

foreach my $key ( keys %key2id )
{
	$db{"key2id:$key"} = $key2id{$key};
}

foreach my $id ( keys %id2name )
{
	my %seen;
	my @unique;
	foreach my $name ( @{$id2name{$id}} )
	{
		next if( defined $seen{$name} );
		$seen{$name} = 1;
		push @unique, $name;
	}
	@{$id2name{$id}} = @unique;

	my $names = join( $separator, @{$id2name{$id}} );
	$names =~ s/^(.*)${separator}(Marc Dequènes)$/$2$separator$1/; # workaround for bug 694081
	$db{"name:$id"} = $names;
}

