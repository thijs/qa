#!/usr/bin/perl -w

# Copyright (C) 2008 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# DB format:
# uv:<package> : <current ubuntu version>
# ub:<package> : <number of ubuntu bugs>

use strict;
use DBD::Pg;
use DB_File;

my $dir= '/etc/ssl/ca-debian';
my $ca_debian = -d $dir ? "--ca-directory=$dir" : '';

my $versions = "wget $ca_debian -q -O - https://udd.debian.org/cgi-bin/ubuntupackages.cgi |";
my $bugs = "wget $ca_debian -q -O - https://udd.debian.org/cgi-bin/ubuntubugs.cgi |";

my $db_filename = "ubuntu-new.db";

my %db;

my $dbh = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT, 0666, $DB_BTREE
	or die "Can't open database $db_filename : $!";

open F, $versions or die "$versions: $!";
while (<F>) {
	next unless (/(\S+) (\S+)/);
	$db{"uv:$1"} = $2;
}
close F;

open F, $bugs or die "$bugs: $!";
while (<F>) {
	next unless (/(\S+)\|(\d+)\|(\d+)/);
	$db{"ub:$1"} = $2;
	$db{"up:$1"} = $3;
}
close F;
