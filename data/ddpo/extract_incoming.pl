#!/usr/bin/perl -w

# Copyright (C) 2005-2017 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# get packages list from incoming/new/deferred/backports
#
# key format:
# i[ue]:<source> "<version>"
# i[ue]-url:<source> "https://incoming.debian.org/...*.dsc"
# d[ue]:<source> "deferred/<days>: <unstable/experimental version>"
# d[ue]-title:<source> "<date> by <changed-by>[ (Uploader: <uploader>)"
# d[ue]-url:<source> "https://ftp-master.debian.org/deferred/"
# n[ue]:<source> "new: <unstable/experimental version>"
# n[ue]-title:<source> "<date> by <changed-by>[ (Uploader: <uploader>)"
# n[ue]-url:<source> "<date> by <changed-by>[ (Uploader: <uploader>)"
# bpo:<source> "<backports version>"
# bpo-title:<source> "<backports version>"
# bpo-url:<source> "<backports directory URL>"
# e@ma.il "<package> ..."
# bpo:e@ma.il "<package> ..."

use strict;
use DB_File;
use Dpkg::Version;
use Mail::Header;
use URI::Escape;

sub redefined_version_compare
{
	my $left = shift;
	my $right = shift;

	foreach my $bad_char ( '_', '/' )
	{
		return 0 if( $left =~ /$bad_char/ );
		return 0 if( $right =~ /$bad_char/ );
	}

	foreach my $subst ( 's/%2B/+/g', 's/%7E/~/g' )
	{
		eval( '$left =~ '.$subst );
		eval( '$right =~ '.$subst );
	}

	return Dpkg::Version::version_compare( $left, $right );
}

my $dir= '/etc/ssl/ca-debian';
my $ca_debian = -d $dir ? "--ca-directory=$dir" : '';

# configuration
my $db_filename = "incoming-new.db";
my $queue_summary = "https://ftp-master.debian.org/new.822";
my $bpo_archive = "ftp.debian.org";
my $bpo_url = "http://ftp.debian.org/debian";

# global variables
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
    or die "Can't open database $db_filename : $!";
my (%packages, %backports);
my %uid;

# extract keyids
open PSQL, "psql -c 'select fingerprint, uid.uid from fingerprint left join uid on fingerprint.uid = uid.id' service=projectb |" or warn "psql: $!";
# below once postgresql version 8.1 is not used anymore on merkel
#open PSQL, "psql -c 'select fingerprint, uid.uid from fingerprint left join uid on fingerprint.uid = uid.id' service=dak |" or warn "psql: $!";
while (<PSQL>) {
    /([0-9A-F]{16})\s+\|\s+(\S+)/ or next;
    $uid{$1} = $2;
}

# mouseover title
sub mouseover_date
{
    my ($date, $changedby, $uploader) = @_;
    $date =~ s/^.*?(\d{1,2} \w{3} \d{4}).*/$1/;
    $changedby =~ s/&/&amp;/g; # remove meta chars
    $changedby =~ s/"/&quot;/g;
    my $ret = "$date by $changedby";
    my $q_uploader = quotemeta ($uploader) if $uploader;
    if ($uploader and $changedby !~ /<$q_uploader\@/) { # assume matching local part implies same person
	$ret .= " (Uploader: $uploader)";
    }
    return $ret;
}

# incoming
for my $dist (qw{unstable experimental}) {
    my $d = substr ($dist, 0, 1);

    open B, "wget $ca_debian -q -O - https://incoming.debian.org/debian-buildd/dists/buildd-$dist/main/source/Sources.xz | unxz |" or die "wget: $!";
    while (!eof(B)) {
	my $reader = new Mail::Header \*B;
	next unless my $package = $reader->get("Package"); chomp $package;
	next unless my $version = $reader->get("Version"); chomp $version;
	next unless my $directory = $reader->get("Directory"); chomp $directory;
	next unless my $maintainer = $reader->get("Maintainer"); chomp $maintainer;
	my $uploaders = $reader->get("Uploaders:");

	$db{"i${d}:$package"} = $version;
	$db{"i${d}-title:$package"} = "incoming $dist";
	$db{"i${d}-url:$package"} = "https://incoming.debian.org/debian-buildd/$directory/" . uri_escape ("${package}_$version.dsc");

	$maintainer =~ s/.*<(.*)>.*/$1/;
	$packages{lc $maintainer}->{$package} = 1;
	if ($uploaders) {
	    chomp $uploaders;
	    foreach (split /, */, $uploaders) {
		s/.*<(.*)>.*/$1/;
		$packages{lc $1}->{$package} = 1;
	    }
	}
    }
    close B;
}

# deferred
open D, "wget $ca_debian -q -O - https://ftp-master.debian.org/deferred/status |" or die "wget: $!";
my %packages_by_fpr;
while (!eof(D)) {
    my $reader = new Mail::Header \*D;
    my $distribution = $reader->get("Distribution"); chomp $distribution;
    my $until = $reader->get("Delayed-Until"); chomp $until;
    my $fingerprint = $reader->get("Fingerprint"); chomp $fingerprint;
    my $source = $reader->get("Source"); chomp $source;
    my $version = $reader->get("Version"); chomp $version;
    my $date = $reader->get("Date"); chomp $date;
    my $changedby = $reader->get("Changed-By"); chomp $changedby;
    my $maintainer = $reader->get("Maintainer"); chomp $maintainer;
    my $uploader = $reader->get("Uploader");
    if ($uploader) {
	chomp $uploader;
	$uploader = $uid{$uploader} if $uid{$uploader};
    }

    my $d = $distribution eq "experimental" ? 'e' : 'u';
    $db{"d${d}:$source"} = "deferred:&nbsp;$version";
    $db{"d${d}-title:$source"} =
	mouseover_date($date, $changedby ." (deferred until $until)", $uploader);
    $db{"d${d}-url:$source"} = "https://ftp-master.debian.org/deferred/";
    $changedby =~ s/.*<(.*)>.*/$1/;
    $maintainer =~ s/.*<(.*)>.*/$1/;
    $packages{lc $changedby}->{$source} = 1;
    $packages{lc $maintainer}->{$source} = 1;
    $packages{"$uploader\@debian.org"}->{$source} = 1 if $uploader;
    $packages_by_fpr{uc($fingerprint)}{$source} = 1;
}
foreach my $fingerprint ( keys %packages_by_fpr )
{
    $db{"fpr:$fingerprint"} = join ' ', keys %{$packages_by_fpr{$fingerprint}};
}
close D;

# new
open N, "wget $ca_debian -q -O - $queue_summary |" or die "wget: $!";
while (!eof(N)) {
    my $reader = new Mail::Header \*N;
    next unless my $architecture = $reader->get("Architectures"); chomp $architecture;
    next if $architecture !~ /\bsource\b/; # skip bin-only changes from buildds
    next unless my $distribution = $reader->get("Distribution"); chomp $distribution;
    next unless my $queue = $reader->get("Queue"); chomp $queue;
    next unless my $source = $reader->get("Source"); chomp $source;
    next unless my $version = $reader->get("Version"); chomp $version;
    my $noepochversion = $version; $noepochversion =~ s/^\d+://;
    my $date;
    if ($reader->get("Date")) {
	$date = $reader->get("Date");
	chomp $date;
    } elsif ($reader->get("Age")) { # fall back to age as long as date is not present
	$date = $reader->get("Age");
	chomp $date;
	$date .= " ago";
    } else {
	next;
    }
    next unless my $changedby = $reader->get("Changed-By"); chomp $changedby;
    next unless my $maintainer = $reader->get("Maintainer"); chomp $maintainer;
    next unless my $uploader = $reader->get("Sponsored-By") || $reader->get("Changed-By");
    chomp $uploader;
    next unless my $changes = $reader->get("Changes-File"); chomp $changes;
    #$uploader = $uid{$uploader} if $uid{$uploader};
    $uploader .= '@debian.org' unless $uploader =~ /@/;

    my $d = ($distribution =~ m/^oldstable(?:-proposed-updates)?$/) ? 'o' :
	    ($distribution =~ m/^(?:stable|proposed-updates)$/) ? 's' :
	    ($distribution eq "testing") ? 't' :
	    ($distribution eq "experimental") ? 'e' : 'u'; # map unknown to unstable
    if ($queue eq "proposedupdates") { # proposed stable updates
	$db{"pu_new:$source"} = "p-u-new:&nbsp;$version";
	$db{"pu_new-title:$source"} = mouseover_date($date, $changedby, $uploader);
	#$db{"pu_new-url:$source"} = "http://release.debian.org/proposed-updates/stable_diffs/" . uri_escape ("${source}_$noepochversion.debdiff");
	$db{"pu_new-url:$source"} = "http://release.debian.org/proposed-updates/stable.html";
    } elsif ($queue eq "oldproposedupdates") { # proposed oldstable updates
	$db{"os-pu_new:$source"} = "os-p-u-new:&nbsp;$version";
	$db{"os-pu_new-title:$source"} = mouseover_date($date, $changedby, $uploader);
	#$db{"os-pu_new-url:$source"} = "http://release.debian.org/proposed-updates/oldstable_diffs/" . uri_escape ("${source}_$noepochversion.debdiff");
	$db{"os-pu_new-url:$source"} = "http://release.debian.org/proposed-updates/oldstable.html";
    } else {
	my @versions = sort Dpkg::Version::version_compare split(/\s+/, $version);
	my $newest = $versions[-1];
	$db{"n${d}:$source"} = "$queue:&nbsp;$newest";
	$db{"n${d}-title:$source"} = mouseover_date($date, $changedby, $uploader);
	my $url = "https://ftp-master.debian.org/new/" . uri_escape ("${source}_$newest.html");
	$db{"n${d}-url:$source"} = $url;
    }
    $changedby =~ s/.*<(.*)>.*/$1/;
    $maintainer =~ s/.*<(.*)>.*/$1/;
    $packages{lc $changedby}->{$source} = 1;
    $packages{lc $maintainer}->{$source} = 1;
    $packages{$uploader}->{$source} = 1;
}
close N;

my %active_dists;
open R, "/srv/qa.debian.org/data/RELEASES" or die "/srv/qa.debian.org/data/RELEASES: $!";
while (<R>) {
    # possible line: 'oldstable  etch  archived'
    next unless (/^(\S+)\s+(\S+)(?:\s+(\S+))?/);
    my ($dist, $codename, $flags) = ($1, $2, $3);
    next if ($flags and $flags =~ /archived/);
    $active_dists{$dist} = $codename;
}

# backports
for my $distkey (keys %active_dists) {
    next if ($distkey =~ /^(experimental|unstable|testing)/);
    for my $bpolevel (qw(backports backports-sloppy)) {
	my $dist = "$active_dists{$distkey}-$bpolevel";
	my $tag = $distkey eq "stable" ? "bpo" : "$distkey-bpo";
	$tag .= "-sloppy" if ($bpolevel =~ /sloppy/);

	my $files_to_zcat = '';
	foreach my $file_to_zcat ( glob "/srv/qa.debian.org/data/ftp/$bpo_archive/dists/$dist/{main,contrib,non-free}/source/Sources.gz" )
	{
	    $files_to_zcat .= " $file_to_zcat" if( -e $file_to_zcat );
	}
	next if( $files_to_zcat eq '' );

	open B, "bash -c 'zcat $files_to_zcat' |" or die "wget: $!";
	while (!eof(B)) {
	    my $reader = new Mail::Header \*B;
	    next unless my $package = $reader->get("Package"); chomp $package;
	    next unless my $version = $reader->get("Version"); chomp $version;
	    next unless my $directory = $reader->get("Directory"); chomp $directory;
	    next unless my $maintainer = $reader->get("Maintainer"); chomp $maintainer;
	    my $uploaders = $reader->get("Uploaders:");
	    $maintainer =~ s/.*<(.*)>.*/$1/;

	    $db{"$tag:$package"} = $version
	    if( not defined $db{"$tag:$package"}
		    or redefined_version_compare( $db{"$tag:$package"}, $version ) < 0 );
	    $db{"$tag-title:$package"} = "$dist";
	    $db{"$tag-url:$package"} = "$bpo_url/$directory/";
	    $backports{lc $maintainer}->{$package} = 1;
	    if ($uploaders) {
		chomp $uploaders;
		foreach (split /, */, $uploaders) {
		    s/.*<(.*)>.*/$1/;
		    $backports{lc $1}->{$package} = 1;
		}
	    }
	}
	close B;
    }
}

sub remove_duplicates
{
	my $value = shift;

	$value =~ s/^ *//;
	$value =~ s/ *$//;
	$value =~ s/  */ /g;

	my %entry;

	foreach my $entry ( split( / /, $value ) )
	{
		$entry{$entry} = 1;
	}

	$value = "";

	foreach my $entry ( sort keys %entry )
	{
		$value .= " $entry";
	}

	$value =~ s/^ *//;

	return $value;
}

my $emailchars = 'a-zA-Z0-9._+\-';

# write package list for "pending uploads"
foreach my $key (keys %packages) {
    my $email = $key;
    $email =~ s/^[^<>]+ <([$emailchars]+\@[$emailchars]+)>$/$1/;
    $email = lc( $email );
    my $value = join ' ', sort keys %{$packages{$key}};
    $value .= " " . $db{$email} if( defined $db{$email} );
    $db{$email} = remove_duplicates( $value );
}

# write package list for "backports"
foreach my $key (keys %backports) {
    my $value = join ' ', sort keys %{$backports{$key}};
    $value .= " " . $db{"bpo:$key"} if( defined $db{"bpo:$key"} );
    $db{"bpo:$key"} = remove_duplicates( $value );
}

# vim:sw=4:
