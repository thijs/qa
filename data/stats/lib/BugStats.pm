# Copyright 2004 Frank Lichtenheld <djpig@debian.org>

package BugStats;

use strict;
use warnings;

use Exporter;

our @ISA = qw( Exporter );
our @EXPORT = qw( get_bug_status get_bug_stats );

our $BUG_STATS_FILE = '/srv/qa.debian.org/data/ddpo/bugs.txt';

my %bug_stats;

sub init {
    
    %bug_stats = ();

    open BUGS_STAT, "<", $BUG_STATS_FILE
	or die "Can't open $BUG_STATS_FILE: $!";

    while (my $bug_str = <BUGS_STAT>) {
	chomp $bug_str;

	my ($pkg,$bug_str) = split /:/, $bug_str;
	my @bug_count = split /\s+/, $bug_str;
	foreach (@bug_count) {
	    $_ =~ s/^(\d+)\(\d+\)$/$1/o;
	}
	my $stat_val = $bug_count[0]*20 + $bug_count[1]*0.5
	    + $bug_count[2]*0.01;
	
#	print STDERR "(BugStats) $pkg\t=>\t$stat_val,\t$bug_str\n";

	$bug_stats{$pkg} = { status => $stat_val,
			     stats => \@bug_count,
			     string => $bug_str,
			 }
    }

    close BUGS_STAT or warn;
}

sub get_bug_status {   
    return $bug_stats{$_[0]}{status};
}

sub get_bug_stats {
    return @{$bug_stats{$_[0]}{stats}};
}

1;
