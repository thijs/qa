#!/usr/bin/perl -w

# buildd.pl - extract buildd data from UDD for use in developers.php

# Copyright (c) 2013 Joachim Breitner <nomeata@debian.org>
# Copyright (c) 2014 Christoph Berg <myon@debian.org>


# This script puts formatted html code in the berkely db. This reduces the load
# on the webserver when generating the pages, and makes it easier to get a
# consistent layout on different pages (e.g. developer.php and PTS).


# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

use strict;
use warnings;

use DBI;
use DB_File;

my $dbh = DBI->connect( "dbi:Pg:service=udd" ) or die;

my $db_filename = "/srv/qa.debian.org/data/buildd/buildd";

my %db;
my $db_file = tie %db, "DB_File", "$db_filename.new.db", O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
        or die "Can't open database $db_filename.new.db : $!";

my @suites = ('sid', 'experimental');
my %goodstate = (
        'Successful' => 1,
        'Installed' => 1,
);
my %okstate = (
        'Successful' => 1,
        'Installed' => 1,
        'Not-For-Us' => 1,
        'Auto-Not-For-Us' => 1,
);
my %wbstate = (
        'BD-Uninstallable' => '∉',
        'Build-Attempted' => '∿',
        'Building' => '⚒',
        'Maybe-Failed' => '✘', # originally '(✘)'
        'Successful' => '✔', #originally '(✔)'
        'Built' => '☺',
        'Failed' => '✘',
        'Failed-Removed' => '✘',
        'Dep-Wait' => '⌚',
        'Installed' => '✔',
        'Needs-Build' => '⌂',
        'Uploaded' => '♐',
        'Not-For-Us' => '⎇',
        'Auto-Not-For-Us' => '⎇', # originally  ' '
);

my $query = "SELECT
               source,
               distribution,
               architecture,
               state,
               state_change
             FROM
               wannabuild
             WHERE
               distribution = ?
             ORDER BY
               source,
               architecture";


for my $suite ('sid','experimental') {
    my $in = '';
    if ($suite ne 'sid') { $in = sprintf " in %s", $suite };

    my $sth = $dbh->prepare( $query );
    $sth->execute($suite) or die;

    my $source;
    my @symbols = ();

    my $count = 0;
    my $bad = 0;
    my $good = 0;

    # I guess I am a functional programmer by heart
    my $write_out = sub {
        if ($bad) {
            if (@symbols > 6) {
                splice @symbols, (@symbols + 1) / 2, 0, '<br />'; # insert break in the middle
            }
            $db{"$source:$suite"} = join '', @symbols;
        } else {
            $db{"$source:$suite"} = sprintf
                '<span class="buildd-summary-state" title="Package in good shape on %d architecture%s%s">%d×<span class="buildd-state buildd-state-Installed">✓</span></span>',
                $good,
                $good == 1 ? '' : 's',
                $in,
                $good;
        }
        $source = undef;
        @symbols = ();
        $count = $bad = $good = 0;
    };

    while( my $row = $sth->fetchrow_hashref ) {
        $write_out->() if (defined $source and $source ne $row->{source});

        $count++;
        $good++ if $goodstate{$row->{state}};
        $bad++ unless $okstate{$row->{state}};
        $source = $row->{source};
        # strip seconds
        my ($date) = $row->{state_change} =~ m/^(\d{4}-\d{2}-\d{2} \d{2}:\d{2})/;
        push @symbols, sprintf '<span class="buildd-state buildd-state-%s" title="%s on %s%s since %s">%s</span>',
            $row->{state}, $row->{state}, $row->{architecture}, $in, $date, $wbstate{$row->{state}};
    }
    $write_out->() if (defined $source);
}


rename "$db_filename.new.db", "$db_filename.db";


# vim: ts=4:expandtab:shiftwidth=4:
